import time
import serial
import serial.tools.list_ports
from colorama import Fore
from colorama import Style
from prometheus_client import Gauge, start_http_server


READ_INTERVAL = 10 # меняет кол-во секунд между опросами датчика


# Create Prometheus gauges for humidity and temperature in
# Celsius
gh = Gauge('dht22_humidity_percent',
           'Humidity percentage measured by the DHT22 Sensor')
gt = Gauge('dht22_temperature',
           'Temperature measured by the DHT22 Sensor', ['scale'])
gf = Gauge('flood_status',
           'flood status from sensor')

# Initialize the labels for the temperature scale
gt.labels('celsius')





def port_define():
    ports = list(serial.tools.list_ports.comports())
    for i in ports:
        j = str(i).split()
        if '/dev/ttyUSB0' in j:
            print(f"serial port was defined as {Fore.GREEN}{i}{Style.RESET_ALL}")
            print(f"Listening port: {Fore.GREEN}{j[0]}{Style.RESET_ALL}")
            return j[0]



def read_device():
    try:
        with serial.Serial() as ser:
            ser.port = port_define()
            # ser.port = "/dev/ttyS0"
            if ser.port is None:
                raise ValueError(f"{Fore.RED}Serial port not found. Reconnecting...{Style.RESET_ALL}")
            ser.baudrate = 9600
            ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
            ser.parity = serial.PARITY_NONE  # set parity check: no parity
            ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
            # ser.timeout = None          #block read
            # ser.timeout = 0             #non-block read
            # ser.timeout = 2              #timeout block read
            ser.xonxoff = False  # disable software flow control
            ser.rtscts = False  # disable hardware (RTS/CTS) flow control
            ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
            ser.writeTimeout = 4  # timeout for write
            try:
                ser.open()
                print("Port has been opened correct")
                result = ser.readline()
                return result
            except Exception as e:
                print(f"error open serial port: {Fore.RED}{str(e)}{Style.RESET_ALL}")
    except Exception as e:
        print(f'{Fore.RED}{e}{Style.RESET_ALL}')
        return None

def check_flood(flood):
    if flood != b'0':
        print(f'{Fore.RED}ALARM!FLOOD DETECTED{Style.RESET_ALL}')
        return 1
    else:
        return 0

def read_sensor():
    while True:
        result = read_device()
        if result:
            try:
                r = result.split()
                humidity, temperature = r[-5], r[-2]
                gh.set(humidity)
                gt.labels('celsius').set(temperature)
                gf.set(check_flood(result.split()[0]))
            except IndexError as e:
                print(f"{Fore.RED}Error reading data: {str(e)}{Style.RESET_ALL}. Retrying...")
            except serial.SerialException as se:
                print(f"{Fore.RED}Serial Exception: {str(se)}{Style.RESET_ALL}. Retrying...")
            except serial.SerialTimeoutException as ste:
                print(f"{Fore.RED}Serial Timeout Exception: {str(ste)}{Style.RESET_ALL}. Retrying...")
        else:
            print(f"{Fore.RED}No data received. Retrying...{Style.RESET_ALL}")
        
        time.sleep(READ_INTERVAL)


if __name__ == "__main__":
    # Expose metrics
    metrics_port = 9101
    start_http_server(metrics_port)
    print(f"Serving sensor metrics on :{Fore.BLUE}{metrics_port}{Style.RESET_ALL}")


    while True:
        read_sensor()
