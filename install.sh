cd /home

apt install git -y
apt install python3-venv -y

git clone https://gitlab.com/JustLowSkill/hut.git
cd /home/hut
chmod +x /home/hut/install.sh

python3 -m venv venv

# Activate the virtual environment
source /home/hut/venv/bin/activate

# Устанавливаем зависимости
/home/hut/venv/bin/python -m pip install -r /home/hut/programm/requirements.txt

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CURRENT_USER=$USER
PYTHON_PATH=$SCRIPT_DIR/venv/bin/python3

# Создаем systemd unit файл
cat << EOF > /etc/systemd/system/hut.service
[Unit]
Description=humidity temperature sensor
After=network.target

[Service]
User=$CURRENT_USER
WorkingDirectory=$SCRIPT_DIR
ExecStart=$PYTHON_PATH $SCRIPT_DIR/programm/main.py
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Перезагружаем systemd
systemctl daemon-reload

# Включаем автозапуск
systemctl enable hut.service

# Запускаем сервис
systemctl start hut.service

echo metrics served on 9101 port